#!/bin/bash

# Requires a conda environment that has geni installed
# ! Active this environment before running this script

geni_path=`python -c "exec(\"import geni\nprint(geni.__file__)\")"`
geni_path=`dirname $geni_path`
echo "Modifying file:" $geni_path

# Need to look for pattern: open(ucpath, "wb+")
# To simplify sed expression, we will look for "wb+" and replace it with "w+"

file_to_update="$geni_path/aggregate/context.py"

echo "Looking for pattern that needs to be replaced (could return empty list):"
grep -n "\"wb+\"" $file_to_update
echo "---"

sed -i -e 's/"wb+"/"w+"/g' $file_to_update

echo "Looking for pattern with which original was replaced (could return empty list):"
grep -n "\"w+\"" $file_to_update
echo "Done!"
