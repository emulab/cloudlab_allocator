import logging

# Subprocess functions
from subprocess import Popen, PIPE, STDOUT, run

# dataframe libraries
import pandas as pd
import numpy as np

# uuid library
import uuid

from configparser import ConfigParser

from orchestration import parseArgs, parse_config, allocate_nodes, deallocate_nodes

def configure_logging(debug=False):
    frmt_str = '%(asctime)s %(name)-12s %(levelname)-8s %(message)s'

    logger = logging.getLogger(__name__)

    # define a handler for console
    console = logging.StreamHandler()
    logger.setLevel(logging.DEBUG if debug else logging.INFO)
    formatter = logging.Formatter(frmt_str)
    console.setFormatter(formatter)
    logger.addHandler(console)

    return logger

LOG = configure_logging(debug=True)

if __name__ == "__main__":

    args = parseArgs()
    nslices = args.nslices
    site = args.site
    hwtype = args.hwtype

    user, project, certificate, private_key, public_key, geni_cache = \
        parse_config("cloudlab.config")

    allocation = allocate_nodes(nslices, site, hwtype, user, project, \
                                certificate, private_key, public_key, \
                                geni_cache, \
                                LOG)

    LOG.info("Done allocating nodes. Proceeding to deallocating.")

    deallocate_nodes(allocation, LOG)
