# Code for Allocating Nodes on CloudLab

This repo contains the code needed to allocate and deallocated nodes on the CloudLab testbed. One current application of this code is in OrderSage tool for studying performance effects coming varying experiment ordering  (repo: [https://gitlab.flux.utah.edu/carina/sigmetrics-tool](https://gitlab.flux.utah.edu/carina/sigmetrics-tool))

To start, `git clone` this repo (for OrederSage, clone it *inside* the directory that contains `sigmetrics-tool` code).

## Prerequisite 1: geni-lib

This code relies on `geni-lib` code for interacting with CloudLab.  The easiest way to install `geni-lib` is to do:

```
pip install geni-lib
```

For the reference, a recently tested instance used the following versions:

```
# pip freeze | grep geni
geni-lib==0.9.9.4
# python --version
Python 3.8.5
```

The version that is installed using `pip install geni-lib` has some differences with the version that was used with Python 2.
To elimianate these differences, do the following:
1. Activate conda environment that has `geni-lib` installed. For instance, `sigmetrics-tool` repo has environment.yml that builds `sigmetrics-tool` environment, and if you have it on the machine where you are running this code, activate it:
```
conda activate sigmetrics-tool
```
(If you need to create this environment, see README file inside the `sigmetrics-tool` repo).
2. Run the script that is part of this repo:
```
bash geni_fix.sh
```
The script's output will show what it is looking for & replacing in `geni-lib` scripts. It should be safe to run this fix script multiple times.

Note about `conda`: if you don't currently use it, to install it follow instructions from: [https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html](https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html)

## Prerequisite 2: CloudLab Credentials

In order to use geni-lib, you will need to have a set of geni credentials ready.  You can get these credentials from the CloudLab portal, but you will need to make some changes before you're able to use them with a geni-lib script.  Namely, the downloaded credential has a password-locked private key inside of it and geni-lib is not set up to use that, therefore you will need to decrypt the private key and combine the resulting key with the certificate in the downloaded file. You will need to perform the following steps:
1. Log on to your CloudLab account, click on your name in the top right corner, and click “Download Credentials” in the pulldown menu.
2. Rename the downloaded file to `cloudlab-enc.pem`.
3. If the `.ssl` directory doesn't already exist in your home directory, then run the following:
```
mkdir ~/.ssl
chmod 700 ~/.ssl
```
4. Move `cloudlab-enc.pem` into `~/.ssl` and then run the following set of commands (when prompted for your password, put in your CloudLab password):
```
cd ~/.ssl
split -p "-----BEGIN CERTIFICATE-----" cloudlab-enc.pem cloudlab-
```
**IF THE ABOVE COMMAND WORKS, RUN THESE:**
```
openssl pkey -in cloudlab-aa -out cloudlab.pem
cat cloudlab-ab >> cloudlab.pem
```
**OTHERWISE, RUN THESE:**
```
csplit -s -z -f cloudlab- cloudlab-enc.pem '/-----BEGIN CERTIFICATE-----/' '{*}'
openssl pkey -in cloudlab-00 -out cloudlab.pem
cat cloudlab-01 >> cloudlab.pem
```
**FINALLY:**
```
chmod 600 cloudlab.pem
rm cloudlab-*
```
Note, you will likely need to run the above sequence one command at a time.  The reason for the separate `split` and `csplit` sections is I noticed that different operating systems could run one but not the other.  If neither the `split` nor `csplit` commands work, you will need to manually copy everything before the `-----BEGIN CERTIFICATE-----` line into a separate file, run the `openssl` command to decrypt it, then copy everything including and after `-----BEGIN CERTIFICATE-----` into that decrypted file.  I was having difficulty running the whole copypasted block as I was testing this out.  I also noticed difficulties running this on my mac laptop, where the `openssl` command would not decrypt the key properly.  If this is the case, you can try running all of this inside of a CloudLab node running one of the standard images, and copy out the resulting `cloudlab.pem` file when you are done to be put on the machine you are planning on running this code from.

## Running

To run the code with the config options specified in `cloudlab.config`, do (from within appropriate Python/conda environment):

```
python test.py -n 1 -s utah -t m510
```

This will allocate & deallocate a single `m510` machine on the Utah cluster. The list of supported hardware types is:
```
['m510', 'm400', 'xl170', 'd6515', 'c6525-25g', 'c6525-100g', 'c220g1', 'c220g2',
'c220g5', 'c8220', 'c6320', 'c6420', 'r320', 'c6220', 'd430']
```

When choosing a particular hardware type, it is important to use the corresponding CloudLab site with the `-s` option. List of available sites:
```
["utah", "wisc", "clemson", "apt", "emulab"]
```
